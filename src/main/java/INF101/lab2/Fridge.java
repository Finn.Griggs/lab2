package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    
    private int maxCapacity;
    private ArrayList<FridgeItem> items = new ArrayList<>();


    public Fridge() {
        this.maxCapacity = 20;
    }

    public int nItemsInFridge() {
        return items.size();
    }

    public int totalSize() {
        return maxCapacity;
    }

    public boolean placeIn(FridgeItem item) {
        if(nItemsInFridge() < totalSize()) {
            items.add(item);
            return true;
        }
        
        return false;
    }

    public void takeOut(FridgeItem item) throws NoSuchElementException {
        int index = items.indexOf(item); 

        if(index >= 0) {
            items.remove(index);
        } else {
            throw new NoSuchElementException();
        }

        
        
    }

    public void emptyFridge() {
        items.clear();
    }

    public ArrayList<FridgeItem> removeExpiredFood(){ 
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            if(items.get(i).hasExpired()) {
                expiredFood.add(items.remove(i));
                i--;
            }
        }

        return expiredFood;
    }

}
